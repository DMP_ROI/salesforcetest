/**
 * Created by JYuen on 5/9/2017.
 */
import express from 'express';
import path from 'path';
import fileSystem from 'fs';
import request from 'request';
import SalesForce from './js/salesforce.js';
var bodyParser = require('body-parser');

var sf = new SalesForce();
var app = express();
process.env.DEV_PORT=33333;
process.env.captchaSK="6LelCCEUAAAAAIuDSEd5DuRXVJJ8xXAagagcA0hj";
var options ={
    root:__dirname,
    dotfiles:'deny',
    headers:{
        'x-timestamp': Date.now(),
        'x-sent':true
    }

};
app.set('view engine', '.html');
app.use(express.static(path.join(__dirname,'public')));

app.use(bodyParser.urlencoded({
    extended: true
}));

/**bodyParser.json(options)
 * Parses the text as JSON and exposes the resulting object on req.body.
 */
app.use(bodyParser.json());
app.get('/',function(req,res,next){
    // var filePath = path.join(__dirname, 'Calculator.html');
    var filePath = path.join(__dirname, 'Captcha.html');
    var stat = fileSystem.statSync(filePath);
    res.writeHead(200, {
        'Content-Type': 'text/html',
        'Content-Length': stat.size
    });

    var readStream = fileSystem.createReadStream(filePath);
    // We replaced all the event handlers with a simple call to readStream.pipe()
    readStream.pipe(res);
    next();
});
app.get('/ROI/:id',function(req,res,next){
    console.log(req.params.id);
    if(req.params.id ==="Success"){
        var filePath = path.join(__dirname, 'ROI.html');

    }
    else{
        var filePath = path.join(__dirname, 'Error.html');
    }
    var stat = fileSystem.statSync(filePath);
    res.writeHead(200, {
        'Content-Type': 'text/html',
        'Content-Length': stat.size
    });

    var readStream = fileSystem.createReadStream(filePath);
    readStream.pipe(res);
    next();
});

app.get('/Calculator',function(req,res,next){
    console.log(req.params.id);
    var filePath = path.join(__dirname, 'Calculator.html');

    var stat = fileSystem.statSync(filePath);
    res.writeHead(200, {
        'Content-Type': 'text/html',
        'Content-Length': stat.size
    });

    var readStream = fileSystem.createReadStream(filePath);
    readStream.pipe(res);
    next();
});
app.post('/checkCaptcha',function(req,res,next){
    console.log(req.body);
    var success = false;
    request.post('https://www.google.com/recaptcha/api/siteverify',
        {form:{secret:"6LelCCEUAAAAAIuDSEd5DuRXVJJ8xXAagagcA0hj", response:req.body["g-recaptcha-response"]}},
    function(err,httpResponse,body){
        if (err) {
            return console.error('upload failed:', err);
        }
        else {
            console.log('Upload successful!  Server responded with:', body);
            console.log("body::::"+body);
            console.log("req.body::::"+JSON.stringify(req.body));
            var parseBody = JSON.parse(body);
            if (parseBody.success){
                console.log("success");
                res.end("200");
                next();
            }
            else{
                console.log("fail");
            }
        }
    });
    res.end("200");
    next();
    });
    function getROI(){
        request.get("http://localhost:33333/ROI");
        console.log("getting ROI file");
    }



app.post('/addRecord',function(req,res,next){
    console.log(req.body);
    sf.AddRecord(req.body);
    res.end("yes");
    next();
});


app.listen(process.env.DEV_PORT, function(){
    console.log('app is listening on port '+ process.env.DEV_PORT);
});