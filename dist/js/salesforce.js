"use strict";

Object.defineProperty(exports, "__esModule", {
    value: true
});

function _classCallCheck(instance, Constructor) { if (!(instance instanceof Constructor)) { throw new TypeError("Cannot call a class as a function"); } }

var SalesForce = function SalesForce() {
    _classCallCheck(this, SalesForce);

    this.name = "salesforce";
};

exports.default = SalesForce;
//# sourceMappingURL=salesforce.js.map